import React from 'react'
import Star from '../svg/Star';
import { View, StyleSheet } from 'react-native';

export default function Rating(props) {

    const ratingArray = [];

    for (let i = 1; i <= 5; i++) {
        if(i <= props.rating) {
            ratingArray.push(
                <Star 
                    key={i} 
                    gap={props.gap}
                    size={props.size}
                    filled 
                />
            );
        } else {
            ratingArray.push(
                <Star 
                    gap={props.gap}
                    size={props.size}
                    key={i} 
                />
            );
        }
    }

    return (
        <View style={styles.container}>
            {ratingArray.map((item, key) => {
                return item
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    }
});