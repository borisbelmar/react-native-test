import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ImageBackground, ImageComponent } from 'react-native';
import { ThemeContext } from '../context/ThemeContext';
import chroma from 'chroma-js';
import Star from '../svg/Star';
import Rating from './Rating';

export default function MediaItem(props) {

    const context = React.useContext(ThemeContext);
    let style = themedStyle(context.selectedTheme);
    return (
        <View style={style.container}>
            <ImageBackground source={require('../assets/mandalorian.jpeg')} style={style.bgImage}>
                <Text style={style.featured}>Nuevo</Text>
                <Text style={style.time}>22:09</Text>
            </ImageBackground>
            <View style={style.content}>
                <Text style={style.title}>Mandalorian</Text>
                <Text style={style.paragraph}>Candy sugar plum sugar plum cake lips text</Text>
                <View style={style.meta}>
                    <Rating 
                        size="20" 
                        rating="3"
                        gap="5"
                    />
                    <Text style={style.date}>22/12/2019</Text>
                </View>
            </View>
        </View>
    )
}

const themedStyle = theme => {
    return StyleSheet.create({
        container: {
            backgroundColor: chroma(theme.base).brighten(0.5),
            justifyContent: 'center',
            borderRadius: 6,
            alignSelf: 'stretch',
            marginHorizontal: 10,
            overflow: 'hidden',
            position: 'relative',
            marginBottom: 30,
            
        },
        paragraph: {
            color: chroma(theme.invert).brighten(2),
            fontSize: 14,
            marginBottom: 6
        },
        content: {
            padding: 12
        },
        featured: {
            padding: 8,
            backgroundColor: theme.primaryColor,
            alignSelf: 'flex-start',
            fontSize: 14,
            position: 'absolute',
            top: 0,
            left: 0
        },
        title: {
            fontSize: 21,
            fontWeight: '700',
            color: theme.primaryColor,
            marginBottom: 5
        },
        bgImage: {
            height: 200,
            borderRadius: 500        
        },
        time: {
            top: 0,
            right: 0,
            padding: 8,
            backgroundColor: chroma('black').alpha(0.5),
            color: 'white',
            alignSelf: 'flex-start',
            fontSize: 14,
            position: 'absolute',
        },
        meta: {
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginTop: 4,
        },
        date: {
            flex: 1,
            fontSize: 13,
            color: chroma(theme.invert).brighten(3)
        }
    })
};