import React from 'react';
import {TextInput, StyleSheet} from 'react-native';

export default class StatefulTextInput extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hasFocus: false
        }
    }

    onFocus = (style) => {
        this.setState({hasFocus: true})
    }

    onBlur = (style) => {
        this.setState({hasFocus: false})
    }

    sendData = (data) => {
        this.props.receivedData(data);
    }

    render() {
        let style = themedStyle(this.props.theme);
        let watchState = this.props.watchState;
        return (
            <TextInput 
                style={this.state.hasFocus ? [style.textInput, style.onFocus] : style.textInput} 
                placeholder={this.props.placeholder}
                value={this.props.value}
                onChangeText={(text) => this.sendData({[watchState]: text})}
                onFocus={() => this.onFocus(style)}
                onBlur={() => this.onBlur(style)}
            />
        )
    }
}

const themedStyle = theme => {
    return StyleSheet.create({
        textInput: {
            paddingVertical: 10,
            paddingHorizontal: 10,
            backgroundColor: '#FFF',
            borderRadius: 6,
            marginBottom: 16,
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: {width: 0, height: 5},
            shadowRadius: 5,
            shadowOpacity: 0.3,
            borderColor: theme.primaryColor,
            borderWidth: 3
        },
        onFocus: {
            borderColor: theme.secondaryColor
        }
    });
}