import React from 'react';
import { ThemeContext } from '../context/ThemeContext';
import { View, StyleSheet, StatusBar, Platform } from 'react-native';

export default function DefaultLayout(props) {

    const context = React.useContext(ThemeContext);
    let style = themedStyle(context.selectedTheme);
    return (
        <>
            <View style={style.statusBar}>
                <StatusBar barStyle={'light-content'} />
            </View>
            {props.children} 
        </>
    )
}

const themedStyle = (theme) => {
    return StyleSheet.create({
      statusBar: {
        height: statusBarHeight(),
        backgroundColor: theme.primaryColor
      }
    });
}

const statusBarHeight = () => {
    if (Platform.OS === 'ios') {
        return 40;
    } else if (Platform.OS === 'android' && Platform.Version >= 25) {
        return 30;
    } else {
        return 0;
    }
}