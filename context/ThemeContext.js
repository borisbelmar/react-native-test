import React from 'react';
import Themes from '../utils/themes';

export const ThemeContext = React.createContext();

export class ThemeContextProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTheme: getTheme(1),
        }
    }

    setTheme = themeId => {
        this.setState({selectedTheme: getTheme(themeId)});
    }

    getAllThemes = () => {
        return Themes;
    }

    render() {
        return (
            <ThemeContext.Provider
                value={{
                    selectedTheme: this.state.selectedTheme,
                    setTheme: this.setTheme,
                    getAllThemes: this.getAllThemes
                }}
            >
                {this.props.children}
            </ThemeContext.Provider>
        )
    }
}

const getTheme = (themeId) => Themes.filter((theme) => theme.id == themeId)[0];