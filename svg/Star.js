import React from 'react';
import { Text, StyleSheet} from 'react-native';
import Svg, {Polygon, Path} from 'react-native-svg';
import { ThemeContext } from '../context/ThemeContext';

export default function Star(props) {
    const context = React.useContext(ThemeContext);

    if(props.filled) {
        return (
            <Svg 
                style={{
                    flex: 1,
                    marginRight: parseInt(props.gap)
                }}
                fill={context.selectedTheme.primaryColor}
                viewBox="0 0 15 15"
                height={props.size}
                width={props.size}
            >
                <Path d="M7.5 11.7l4.5 2.8-1.2-5.2 4.1-3.5-5.3-.4L7.5.5 5.4 5.4l-5.2.4 4 3.5L3 14.5l4.5-2.8z" />
            </Svg>
        )
    } else {
        return (
            <Svg 
                style={{
                    flex: 1,
                    marginRight: parseInt(props.gap)
                }}
                fill={context.selectedTheme.primaryColor}
                viewBox="0 0 15 15"
                height={props.size}
                width={props.size}
            >
                <Path d="M7.5 10.37L4.74 12l.73-3.14L3 6.77l3.22-.28 1.25-3 1.26 3 3.27.29L9.54 8.9l.73 3.1zm7.35-4.53l-5.28-.46L7.5.52 5.43 5.39l-5.28.45 4 3.48L3 14.48l4.5-2.74 4.5 2.74-1.2-5.16z" />
            </Svg>
        )
    }
}