import React from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import chroma from 'chroma-js';
import _ from 'lodash';
import StatefulTextInput from '../components/StatefulTextInput';
import DefaultLayout from '../layouts/DefaultLayout';
import MediaItem from '../components/MediaItem';
import { ThemeContext } from '../context/ThemeContext';

export default class TestHome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Jhon',
            lastname: 'Snow'
        }
    }

    handleClick = (themeId, e) => {
        context.setTheme(themeId);
    }

    onReceivedData = (data) => {
        this.setState(data);
    }

    render() {
        let style = themedStyle(this.context.selectedTheme);
        return (
            <DefaultLayout>
                <View style={style.container}>
                    <Text style={style.title}>{this.props}</Text>
                    <Text style={style.paragraph}>This is a theme</Text>
                    <StatefulTextInput 
                        placeholder="Ingresa un nombre" 
                        value={this.state.name}
                        theme={this.context.selectedTheme} 
                        receivedData={this.onReceivedData}
                        watchState="name"
                    />
                    <StatefulTextInput 
                        placeholder="Ingres un apellido" 
                        value={this.state.lastname}
                        theme={this.context.selectedTheme} 
                        receivedData={this.onReceivedData}
                        watchState="lastname"
                    />
                    <View style={style.nameCard}>
                        <Text style={style.cardTitle}>Hola {this.state.name} {this.state.lastname}!</Text>
                    </View>
                    <View style={style.row}>
                        <TouchableOpacity style={style.button} onPress={(e) => this.handleClick(1, e)}>
                            <Text style={style.buttonText}>Dark Deep Salmon</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={style.button} onPress={(e) => this.handleClick(2, e)}>
                            <Text style={style.buttonText}>Deep Salmon</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={style.row}>
                        <TouchableOpacity style={style.button} onPress={(e) => this.handleClick(3, e)}>
                            <Text style={style.buttonText}>Lagoon</Text>
                        </TouchableOpacity>
                    </View>
                    <MediaItem />
                </View>
            </DefaultLayout>
        )
    }
}

TestHome.contextType = ThemeContext;

const themedStyle = (theme) => {
    return StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: theme.base,
            padding:30
        },  
        title: {
            fontSize: 24,
            color: theme.primaryColor,
            fontWeight: 'bold',
            marginBottom: 6
        },
        paragraph: {
            fontSize: 16,
            color: theme.invert,
            marginBottom: 16
        },
        nameCard: {
            backgroundColor: '#FFF',
            padding: 20,
            borderRadius: 8,
            marginTop: 16,
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: {width: 0, height: 5},
            shadowRadius: 5,
            shadowOpacity: 0.3
        },
        cardTitle: {
            fontSize: 18
        },
        row: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 20
        },
        button: {
            backgroundColor: theme.primaryColor,
            borderRadius: 6,
            paddingHorizontal: 30,
            alignItems: 'center',
            padding: 10,
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: {width: 0, height: 5},
            shadowRadius: 5,
            shadowOpacity: 0.3
        },
        buttonText: {
            color: 'white',
            fontWeight: 'bold',
            fontSize: 14,
            textAlign: 'center',
        }
    })
}