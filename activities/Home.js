import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import DefaultLayout from '../layouts/DefaultLayout';
import { ThemeContext } from '../context/ThemeContext';
import MediaItem from '../components/MediaItem';

export default function Home(props) {
  
  const context = React.useContext(ThemeContext);

  const handlePress = (themeId) => {
    context.setTheme(themeId);
  }

  let style = themedStyle(context.selectedTheme);
  return (
    <DefaultLayout>
      <View style={style.container}>
        <MediaItem/>
        <Text style={style.paragraph}>Tema Actual: {context.selectedTheme.name}</Text>
        <ScrollView style={{maxHeight: 220, overflow: 'hidden', width: "95%"}}>
          {context.getAllThemes().map((theme, key) => {
            return (
              <TouchableOpacity key={key} style={style.button} onPress={() => handlePress(theme.id)}>
                <Text style={style.buttonText}>{theme.name}</Text>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    </DefaultLayout>
  );
}



const themedStyle = theme => {
  return (
    StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: theme.base,
        alignItems: 'center',
        justifyContent: 'center'
      },
      paragraph: {
        color: theme.invert,
        fontSize: 20,
        marginBottom: 20,
        textAlign: 'center',
      },
      button: {
        backgroundColor: theme.primaryColor,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 6,
        marginBottom: 20,
        alignSelf: 'stretch',
        marginHorizontal: 10,
      },
      buttonText: {
        color: '#FFF',
        fontSize: 21
      }
    })
  )
}