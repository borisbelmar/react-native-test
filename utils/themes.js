const Themes = [
    {
      id: 1,
      name: 'Dark Deep Salmon',
      type: 'dark',
      primaryColor: '#ff8a65',
      secondaryColor: '#90a4ae',
      base: 'hsl(0, 0%, 10%)',
      invert: 'hsl(0, 0%, 95%)'
    },
    {
      id: 2,
      name: 'Deep Salmon',
      type: 'light',
      primaryColor: '#ff8a65',
      secondaryColor: '#90a4ae',
      base: 'hsl(0, 0%, 95%)',
      invert: 'hsl(0, 0%, 5%)'
    },
    {
      id: 3,
      name: 'Lagoon',
      type: 'light',
      primaryColor: '#4db6ac',
      secondaryColor: '#ffa726',
      base: 'hsl(0, 0%, 95%)',
      invert: 'hsl(0, 0%, 5%)'
    },
    {
      id: 4,
      name: 'Mediastream',
      type: 'light',
      primaryColor: 'hsl(78, 100%, 42%)',
      secondaryColor: 'hsl(78, 100%, 42%)',
      base: 'hsl(0, 0%, 95%)',
      invert: 'hsl(0, 0%, 5%)'
    },
    {
      id: 5,
      name: 'Dark Mediastream',
      type: 'dark',
      primaryColor: 'hsl(78, 100%, 42%)',
      secondaryColor: 'hsl(78, 100%, 42%)',
      base: 'hsl(0, 0%, 5%)',
      invert: 'hsl(0, 0%, 95%)'
    }
  ]

export default Themes;