import React from 'react';
import { StyleSheet, Text, View, StatusBar} from 'react-native';
import TestHome from './activities/TestHome';
import Themes from './utils/themes';
import _ from 'lodash';
import { ThemeContextProvider } from './context/ThemeContext';
import Home from './activities/Home';
import MediaItem from './components/MediaItem';

export default class App extends React.Component {

  render() {
    return (
      <ThemeContextProvider>
          <Home />
      </ThemeContextProvider>
    );
  }
}

